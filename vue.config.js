
const fs = require('fs');
module.exports = {
  transpileDependencies: ["vuetify"],

  pluginOptions: {
    i18n: {
      locale: 'fr',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: true
    }},
  devServer:{ public : 'private.swaggit.net',
    host: "0.0.0.0",
    disableHostCheck: true,
    key: fs.readFileSync("/etc/letsencrypt/live/private.swaggit.net/privkey.pem"),
    cert: fs.readFileSync("/etc/letsencrypt/live/private.swaggit.net/fullchain.pem"),

    proxy:{ "^/a" :{
        target:'http://localhost:3334/'},
      "^/api/v1":{
        target: 'http://localhost:3334/api/v1'
      }

    }
  }
}