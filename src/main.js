import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import axios from "axios";
import EventBus from "./plugins/event-bus";


import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";

import Default from "./views/layouts/Default";
import Dashboard from "./views/layouts/Dashboard";
import i18n from './i18n';
import VueCookies from 'vue-cookies';
import AdminLayout from "./views/layouts/Admin"
import "./assets/scss/swaggit.scss";
Vue.use(VueCookies);

Vue.config.productionTip = false;

Vue.prototype.$eventBus = EventBus;
Vue.prototype.$axios = axios;
Vue.$cookies.config('7d');
Vue.$cookies.set('locale', 'en');


axios.defaults.headers = { authorization: `Bearer F2707837BE3913926974CCF77A2CF2AB16A9BA127EEC46D5B7CD70EC18F6024A` };


Vue.component("dashboard-layout", Dashboard);
Vue.component("default-layout", Default);
Vue.component("admin-layout", AdminLayout);


new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App)
}).$mount("#app");
