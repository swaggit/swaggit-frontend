import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/pages/HomePage.vue";
import ProfileSetup from "../components/Setup/Profile";
import Main from "../views/pages/Dashboard/Main";
import AdminMain from "../views/pages/Admin/Main";


Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/profile/setup",
    name: "profile-setup",
    component: ProfileSetup
  },
  {
    path: "/dashboard",
    name: "dashboard",
    component: Main,
    meta: {
      layout: "dashboard",
      requiresAuth: true
    }
  },
  {
    path: "/admin",
    name: "admin",
    component: AdminMain,
    meta: {
      layout: "admin"
    }

  }
  //{
    //path: "/about",
    //name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    //component: () =>
    //  import(/* webpackChunkName: "about" */ "../views/About.vue")
 // }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

/*router.beforeEach( (to, from, next)  => {
  const requiresAuth = to.matched.some(x => x.meta.requiresAuth);

  if(requiresAuth) {
    axios({method: "GET", url: "http://localhost:3333/session", withCredentials: true}).then(session => {

      axios({
        method: "POST",
        url: "http://localhost:3333/session",
        data: {session: session},
        headers: {"content-type": "application/json"},
        withCredentials: true
      }).then(response => {
        if (response.status !== 200) {
          console.log(response);
        } else {
          next();
        }
      }).catch(error => {
        console.log(error);
        next("/");

      });

    });
  }


});*/



export default router;
