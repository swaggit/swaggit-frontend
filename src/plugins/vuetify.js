import Vue from "vue";
import Vuetify from "vuetify/lib";

Vue.use(Vuetify);

import fr from 'vuetify/es5/locale/fr';
import en from 'vuetify/es5/locale/en';
import '../assets/fa/css/all.css';



export default new Vuetify({
    lang: {
        locales: {fr, en},
        current: 'en'
    },
    icons: {
        iconfont: 'fa',
    }
});
