import ErrorComponent from "../components/ErrorPlugin/errors";

const optionsDefaults = {


    data: {
        errorMSG: null,
        errorTitle: null,
        closable: true,
        showErrorPopup:false,
        onError(errorMSG, errorTitle) {
            this.errorMSG = errorMSG;
            this.errorTitle = errorTitle;
            this.showErrorPopup = true;
        }
    }

};


const ErrorPlugin = {
    install(Vue, options) {

     const opts = {...optionsDefaults, ...options};
        Vue.component(ErrorComponent.name, ErrorComponent);

      const root = new Vue({
          data: { errorMSG: null, errorTitle: null, showErrorPopup:false}
      });

      root.$on('error', opts.data.onError);

    Vue.prototype.$errors = root;


    }
};

export default ErrorPlugin;
