const state = {
  isLoading: false,
  isSaving: false,
  error: null,
  showError: false,
  showDemo: true,
  currentLocale: 'en'
};

const mutations = {
  setLoading(state, payload) {
    state.isLoading = payload;
  },
  setError(state, payload) {
    state.error = payload;
    state.showError = true;
  },
  setNoError(state) {
    state.error = null;
    state.showError = false;
  },
  hideDemo(state) {
    state.showDemo = false;
  },
  reset(state) {
    state.isLoading = false;
    state.error = null;
    state.showError = false;
    state.showDemo = true;
  },
  setLocale(state, locale) {
    state.currentLocale = locale;
  },
  setIsSaving(state, payload) {
    state.isSaving = payload;
  }
};

const actions = {
  loading({ commit }) {
    commit("setLoading", true);
  },
  doneLoading({ commit }) {
    commit("setLoading", false);
  },
  resetErrors({ commit }) {
    commit("setNoError");
  },
  setNewError({commit}, payload) {
    commit("setError", payload)
  },
  disableDemoNotice({commit}) {
    commit("hideDemo");
  },
  resetAll({commit}) {
    commit("reset");
  },
  switchLocale({commit}, payload) {
    commit('setLocale', payload);
  }
};

const getters = {
  getLoadingStatus: state => {
    return state.isLoading;
  },
  getError: state => {
    return state.error;
  },
  getShowError: state => {
    return state.showError;
  },
  getShowDemo: state => {
    return state.showDemo;
  },
  getLocale: state => {
    return state.currentLocale;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
