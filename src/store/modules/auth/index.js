import axios from "axios";
import router from "../../../router";
import Vue from "vue";

const state = {

  isAuthenticated: false,
  user: {},
  needProfileSetup: false,
  emailValid: false,
  phoneValid: false,
  userToken: "",
  profile: {},
  sessionID: ""

};

const mutations = {

  setIsAuthenticated(state, payload) {
    state.isAuthenticated = payload;
  },
  setUser(state, payload) {
    state.user = payload;
  },
  setNeedProfileSetup(state, payload) {
    state.needProfileSetup = payload;
  },
  setEmailValid(state, payload) {
    state.emailValid = payload;
  },
  setPhoneValid(state, payload) {
    state.phoneValid = payload;
  },
  setUserToken(state, payload) {
    state.userToken = payload;
  },

  resetAuth(state) {
    state.isAuthenticated = false;
    state.user = {};
    state.needProfileSetup = false;
    state.userToken = "";
  },
  setProfile(state, payload) {
    state.profile = payload;
  },
  setSessionID(state, payload) {
    state.sessionID = payload;
  }

};

const actions = {

  registerNewUser({commit}, payload) {

    axios.post("https://api.swaggit.net/register", payload).then(response => {

      if(response.status === 200) {
        Vue.$cookies.set("email", payload.email);
        commit("setIsAuthenticated", true);
        commit("setNeedProfileSetup", true);
        commit("setUserToken", response.data.token);
        commit("setUser", {id:response.data.userid})
      }

    }).catch(error => {
      commit('setIsAuthenticated', false);
      commit('setUser', "");

      this.dispatch('setError', error);
    })
  },
  resetAuth({commit}) {
    commit('resetAuth');
  },
  login({commit}, payload) {

    axios.post("https://api.swaggit.net/login", payload).then(response => {

      if(response.data) {
        Vue.$cookies.set('session', JSON.stringify(response.data.session));
        Vue.$cookies.set('isLoggedin', true);
        Vue.$cookies.set("email", payload.email);


          commit('setIsAuthenticated', true);
          commit('setProfile', response.data.profile[0]);
          if(response.data.profile[0].gender === "") {
            router.push('/profile/setup');
          } else {
            router.push('/dashboard');
          }




      }

    }).catch(error => {
      commit('setIsAuthenticated', false);
      commit('setUser', "");
      commit('setError', error);
      router.push('/');
    })

  },
  logout({commit}) {
    commit('resetAuth');
    Vue.$cookies.set('session', null);
    Vue.$cookies.set('isLoggedin', false);
    router.push('/');
  },

  profileSetup({commit}, payload) {

    axios.post("https://api.swaggit.net/api/v1/profile/setup", payload).then(response => {

      if(response.status === 200 && response.data.success === true) {
        commit("setProfile", response.data.profile);
        router.push("/dashboard");
      }

    }).catch(error => {

      commit('setError', error);

    })

  }

};

const getters = {
  getAuthStatus: state => {
    return state.isAuthenticated;
  },
  getUserID: state => {
    return state.user;
  },
  getProfileNeedSetup: state => {
    return state.needProfileSetup;
  },
  getCurrentUserProfile: state => {
    return state.profile;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
