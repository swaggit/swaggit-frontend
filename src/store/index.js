import Vue from "vue";
import Vuex from "vuex";

import Persistence from "../plugins/vuex-persist";

import common from "./modules/common";
import auth from "./modules/auth";
//import profileSetup from "./modules/profile/setup";
//import profiles from "./modules/profile/";
//import posts from "./modules/posts";

const debug = process.env.NODE_ENV !== "production";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    common,
    auth,
    /* profileSetup,
     profiles,
     posts*/
  },
  strict: debug,
  plugins: [Persistence.plugin]
});
